*[Home](../../home) > [User guide](../../user-guide) &gt;* Login

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Login

When you open Fedilab for the first time or when you are adding a new account to Fedilab, you have to follow this procedure.<br><br>Here's how you can log in to your **Fediverse** account in **Fedilab**

### 1. Enter your instance

<img src="../../res/guides/login/login.png" width="350px">

- **Instance**<br>In here you have to enter the host name of your instance. For an example if you created your account on https://mastodon.social, You have to type just "**mastodon.social**".<br>The login button will only work if the address you entered is valid and your instance is up.

*If the instance you entered is a* ***Peertube***, ***GNU Social*** *or* ***Friendica*** *instance, You will be taken to the login page in Fedilab*

*If it's a* ***Mastodon*** *or* ***Pleroma*** *instance, You will be taken to your instance's login page (web page).*

*Optionally, if you want to use a separate web browser to continue, click on the three-dot-menu on the top-right corner and check* ***'Custom tabs'*** *option* <br><br> <img src="../../res/guides/login/custom_tabs.png" width="350px">

- Then press the '**Connect**' button<br>&nbsp;


### 2. Login to your account<br>&nbsp;

- [Mastodon / Pleroma](#mastodon-pleroma)
- [Peertube / GNU Social / Friendica](#peertube-gnu-social-friendica)

<a name="mastodon-pleroma"></a><br>
<h4>Mastodon / Pleroma accounts</h4>

After pressing the 'Connect' button you should see the login page of your instance.

<img src="../../res/guides/login/web_based.png" width="600px">

- **Email**<br>Enter the Email address you used to sign up for the account. For **Pleroma** accounts, you can also use the **username** of your account

- **Password**<br>Enter the password for your account

- Press the '**LOG IN**' button.<br>

#### *Two-Factor Authentication (Optional)*
If you don't have two-factor authentication enabled for your account, you won't see this step. In that case [skip this](#authorize)

Now enter your two-factor code.  <br><br> <img src="../../res/guides/login/2fa.png" width="350px">

<a name="authorize"></a><br>
<h4>*Authorize*</h4>

- Then press 'AUTHORIZE' button to give Fedilab access to your account. <br><br> <img src="../../res/guides/login/authorize.png" width="350px">

- Now the login procedure is complete and you will be taken to your account!

<a name="peertube-gnu-social-friendica"></a><br>
<h4>Peertube / GNU Social / Friendica accounts</h4>

After pressing the 'Connect' button you should see the login page of Fedilab.

<img src="../../res/guides/login/app_based.png" width="350px">

- Enter the **Username** and **Password** of your account

- Press the '**Login**' button.<br>

- Now the login procedure is complete and you will be taken to your account!
