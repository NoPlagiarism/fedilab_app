*[Home](../../home) > [Features](../../features) &gt;* Change launcher icon

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Change Launcher Icon
Your phone, Your apps, Your icons


Fedilab got a new logo recently. It was selected by a vote. Normally you would have to accept the popular choice, even if your don't like it. However Fedilab gives you an option to use the logo you like. To change it,

- Open the settings from side menu/navigation drawer<br><br>
<img src="../../res/change_icons/1.png" width="250px"/>

- Go to "Interface" tab. then tap on "Change app icon" button at the bottom<br><br>
<img src="../../res/change_icons/2.png" width="250px"/>

- Now select your favorite icon :)<br><br>
<img src="../../res/change_icons/3.png" width="250px"/>

<br><br><br><br>

#### Possible issues
- I can't find it in my home screen<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*When you change the icon, it's likely that the shortcuts you had created for the old icon gets removed from the home screen. You can add a new shortcut again from the apps list*

- Oh, no. Fedilab completely disappeared from my phone<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Some launchers fail to display the app after changing the icon. If this happened to you, try to clear the cache of your launcher app. This should solve it. Then restart your phone if it still doesn't show up*

<br><br>
